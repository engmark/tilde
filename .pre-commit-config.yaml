---
# Configuration file for pre-commit (https://pre-commit.com/).
# Please run `pre-commit run --all-files` when adding or changing entries.

repos:
  - repo: https://github.com/macisamuele/language-formatters-pre-commit-hooks
    rev: a6273196190bb0f68caf1dc68073cf62c719f725 # frozen: v2.14.0
    hooks:
      - id: pretty-format-ini
        args: [--autofix]

  - repo: https://github.com/pre-commit/pre-commit-hooks
    rev: cef0300fd0fc4d2a87a85fa2093c6b283ea36f4b # frozen: v5.0.0
    hooks:
      - id: pretty-format-json
        args: [--autofix]
      - id: pretty-format-json
        args: [--autofix]
        files: ^\.config/mcomix/.*\.conf$
        types: []

  - repo: https://gitlab.com/engmark/sort-hook
    rev: 4b2564c7602aa1510034659de6cbb341172f42df # frozen: v3
    hooks:
      - id: sort
        args: [--locale=en_NZ.UTF-8]
        files: ^(.*/\.gitignore|\.bash_history|\.config/darktable/(keyboardrc|shortcutsrc|shortcutsrc\.defaults))$
        exclude: ^\.config/\.gitignore$
        stages: [pre-commit]

  - repo: local
    hooks:
      - id: nixfmt
        name: nixfmt
        entry: nixfmt
        files: \.nix$
        language: system
        stages: [pre-commit]

      - id: bash
        name: bash
        entry: bash
        args: [-o, noexec]
        types: [bash]
        language: system
        stages: [pre-commit]

      # TODO: Remove if https://github.com/pre-commit/identify/issues/350 is fixed
      - id: bash_other
        name: bash other
        entry: bash
        args: [-o, noexec]
        files: ^\.(bash_history|envrc|profile)
        language: system
        stages: [pre-commit]

      - id: check-gitlab-ci
        name: check-gitlab-ci
        entry: check-jsonschema
        args: [--builtin-schema, vendor.gitlab-ci, --data-transform, gitlab-ci]
        files: ^\.gitlab-ci.yml$
        language: system
        stages: [pre-commit]

      - id: deadnix
        name: deadnix
        entry: deadnix
        args: [--edit, --fail]
        files: \.nix$
        language: system
        stages: [pre-commit]

      - id: editorconfig-checker
        name: Check .editorconfig rules
        entry: editorconfig-checker
        types: [text]
        require_serial: true
        language: system
        stages: [pre-commit]

      - id: gitlint
        name: gitlint
        entry: gitlint
        args: [--fail-without-commits, --staged, --msg-filename]
        language: system
        stages: [commit-msg]

      - id: pathchk
        name: pathchk
        entry: pathchk
        args: [--portability]
        exclude: ^(\.pre-commit-config\.yaml|\.config/(digikam_systemrc|mcomix/(keybindings|preferences)\.conf|darktable/(shortcutsrc\.defaults|styles/.*\.dtstyle)))$
        language: system
        stages: [pre-commit]

      - id: prettier
        name: Prettier
        entry: prettier
        args: [--ignore-unknown, --list-different, --write]
        types: [text]
        language: system
        stages: [pre-commit]

      - id: shellcheck
        name: shellcheck
        entry: shellcheck
        args: [--enable=all, --external-sources, --severity=style]
        types: [shell]
        language: system
        stages: [pre-commit]

      # TODO: Remove if https://github.com/pre-commit/identify/issues/350 is fixed
      - id: shellcheck_other
        name: shellcheck other
        entry: shellcheck
        args: [--enable=all, --external-sources, --severity=style]
        files: ^\.envrc$
        language: system
        stages: [pre-commit]

      - id: shfmt
        name: Format bash shell scripts
        entry: shfmt
        args: [--case-indent, --indent=4, --list, --space-redirects, --write]
        types: [shell]
        language: system
        stages: [pre-commit]

      # TODO: Remove if https://github.com/pre-commit/identify/issues/350 is fixed
      - id: shfmt
        name: Format bash shell scripts
        entry: shfmt
        args: [--case-indent, --indent=4, --list, --space-redirects, --write]
        files: ^\.envrc$
        language: system
        stages: [pre-commit]

      - id: statix
        name: statix
        entry: statix
        args: [check]
        files: \.nix$
        pass_filenames: false
        language: system
        stages: [pre-commit]

  - repo: https://gitlab.com/engmark/shellcheck-gitlab-ci-scripts-hook
    rev: 48adb4e0b14aad9d657771ff0720b592652d266b # frozen: v2
    hooks:
      - id: shellcheck-gitlab-ci-scripts
        files: ^\.gitlab-ci\.yml$

  - repo: meta
    hooks:
      - id: check-hooks-apply
      - id: check-useless-excludes
